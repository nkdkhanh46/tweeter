package com.martin.tweeter.features.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.martin.tweeter.databinding.ItemMessageBinding
import com.martin.tweeter.models.Message

class MessageAdapter: RecyclerView.Adapter<MessageAdapter.MessageViewHolder>() {
    private val messages: ArrayList<String> = ArrayList()

    override fun onCreateViewHolder(container: ViewGroup, viewType: Int): MessageViewHolder {
        val binding: ItemMessageBinding = ItemMessageBinding.inflate(LayoutInflater.from(container.context), container, false)
        return MessageViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.bind(messages[position])
    }

    fun swapData(messages: ArrayList<String>) {
        this.messages.clear()
        this.messages.addAll(messages)
        notifyDataSetChanged()
    }

    inner class MessageViewHolder(private val binding: ItemMessageBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(message: String) {
            binding.tvMessage.text = message
        }
    }
}