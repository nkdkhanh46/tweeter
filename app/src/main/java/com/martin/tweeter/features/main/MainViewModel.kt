package com.martin.tweeter.features.main

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import javax.inject.Inject

class MainViewModel @Inject constructor(): ViewModel() {

    companion object {
        const val MESSAGE_LENGTH_MAX = 50
        const val INDICATOR_SMALLEST = "1/1"
    }

    val messages = MutableLiveData<ArrayList<String>>()
    val exceedLengthLimitation = MutableLiveData<Boolean>()

    val enableSend = ObservableField(false)

    /**
     * Send new messages if valid
     *
     * @param message: the message user've inputted
     */
    fun sendMessage(message: String) {
        if (enableSend.get() == false) return

        val m = this.messages.value ?: ArrayList()

        val newMessages = splitMessage(message)
        newMessages?.let {
            m.addAll(newMessages)
            this.messages.value = m
        }
    }

    /**
     * Split the message into array of messages (exceed MESSAGE_LENGTH_MAX)
     *
     * @param message
     * @return list of messages if valid, otherwise null
     */
    fun splitMessage(message: String): ArrayList<String>? {
        val result = ArrayList<String>()
        if (message.length <= MESSAGE_LENGTH_MAX) {
            result.add(message)
            return result
        }

        val words = message.split(" ")
        val lengths = words.map { word -> word.length }

        var line = ArrayList<Int>()
        line.add(INDICATOR_SMALLEST.length)
        var lineSum = INDICATOR_SMALLEST.length
        val pairs = ArrayList<Pair<Int, ArrayList<Int>>>()

        for (i in 0 until lengths.size) {
            val length = lengths[i]
            if (length > MESSAGE_LENGTH_MAX || INDICATOR_SMALLEST.length + length + 1 > MESSAGE_LENGTH_MAX) {
                exceedLengthLimitation.value = true
                return null
            }

            if (lineSum + length + 1 > MESSAGE_LENGTH_MAX) {
                pairs.add(Pair(lineSum, line))
                line = ArrayList()
                line.add(INDICATOR_SMALLEST.length)
                line.add(length)
                lineSum = INDICATOR_SMALLEST.length + length + 1
                continue
            }

            line.add(length)
            lineSum += length + 1
            if (i == lengths.size-1) {
                pairs.add(Pair(lineSum, line))
            }
        }

        val indicatedPairs = correctCountIndicator(pairs) ?: run {
            exceedLengthLimitation.value = true
            return null
        }

        var l: String
        var wordIndex = 0
        for (i in 0 until indicatedPairs.size) {
            if (wordIndex >= words.size) break

            val pair = indicatedPairs[i]
            l = "${i+1}/${indicatedPairs.size}"
            for (j in 1 until pair.second.size) {
                if (wordIndex >= words.size) break

                l = "$l ${words[wordIndex]}"
                wordIndex++
            }
            result.add(l)
        }

        return result
    }

    /**
     * Replace temporary indication of each message with the correct one
     *
     * @param data the data need to be corrected
     * @return list of correct data (pair of character count and
     */
    private fun correctCountIndicator(data: ArrayList<Pair<Int, ArrayList<Int>>>): ArrayList<Pair<Int, ArrayList<Int>>>? {
        val firstCount = data.size

        val pairs = ArrayList<Pair<Int, ArrayList<Int>>>()
        val redundant = ArrayList<Int>()
        var redundantSum = 0
        for (i in 0 until data.size) {
            var sum = data[i].first + redundantSum
            val line = data[i].second
            line.addAll(1, redundant)
            redundant.clear()
            redundantSum = 0
            val indicator = "${i+1}/$firstCount"
            if (indicator.length == line[0]) {
                pairs.add(Pair(sum, line))
                continue
            }


            sum = sum - line[0] + indicator.length
            if (sum <= MESSAGE_LENGTH_MAX) {
                line[0] = indicator.length
                pairs.add(Pair(sum, line))
                continue
            }

            line[0] = indicator.length
            for (j in line.size-1 downTo 1) {
                val length = line[j]
                sum -= (length + 1)
                redundant.add(length)
                redundantSum += (length + 1)
                if (redundant.size == line.size-2) return null

                if (sum <= MESSAGE_LENGTH_MAX) {
                    val newLine = ArrayList<Int>()
                    newLine.addAll(line.subList(0, line.size - redundant.size))
                    pairs.add(Pair(sum, newLine))
                    break
                }
            }
        }

        if (pairs.size != firstCount) return correctCountIndicator(pairs)

        return pairs
    }

    fun updateSendButtonState(message: String) {
        enableSend.set(message.isNotEmpty())
    }
}