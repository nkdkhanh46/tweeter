package com.martin.tweeter.features.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import com.martin.tweeter.R
import com.martin.tweeter.application.MainApplication
import com.martin.tweeter.base.BaseActivity
import com.martin.tweeter.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: MessageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupBinding()
        initView()
        setupEvents()
        observeChanges()
    }

    /**
     * Setup view/data binding
     *
     */
    private fun setupBinding() {
        (application as MainApplication).appComponent.inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]
        binding.viewModel = viewModel
        binding.executePendingBindings()
    }

    private fun initView() {
        setupMessagesListView()
    }

    /**
     * Setup RecyclerView which display list of message
     *
     */
    private fun setupMessagesListView() {
        rvMessage.layoutManager = LinearLayoutManager(this)
        adapter = MessageAdapter()
        rvMessage.adapter = adapter
    }

    /**
     * Setup event of all views
     *
     */
    private fun setupEvents() {
        etMessages.addTextChangedListener(object :TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.updateSendButtonState(text.toString())
            }
        })
        btnSend.setOnClickListener {
            viewModel.sendMessage(etMessages.text.toString())
            etMessages.setText("")
        }
    }

    /**
     * Observe changes from live data variables of ViewModel
     *
     */
    private fun observeChanges() {
        viewModel.messages.observe(this, Observer {
            it?.let { messages ->
                adapter.swapData(messages)
            }
        })

        viewModel.exceedLengthLimitation.observe(this, Observer {
            it?.let { exceed ->
                if (exceed) {
                    showMessageExceedLengthLimitationError()
                }
                viewModel.exceedLengthLimitation.value = null
            }
        })
    }

    private fun showMessageExceedLengthLimitationError() {
        AlertDialog.Builder(this)
            .setMessage(R.string.message_contains_invalid_word)
            .setPositiveButton(R.string.ok) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .show()
    }
}
