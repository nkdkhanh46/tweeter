package com.martin.tweeter.dagger

import com.martin.tweeter.base.BaseActivity
import com.martin.tweeter.dagger.modules.AppModule
import com.martin.tweeter.dagger.modules.ViewModelModule
import com.martin.tweeter.features.main.MainActivity
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, ViewModelModule::class])
@Singleton
interface AppComponent {
    fun inject(target: BaseActivity)
    fun inject(target: MainActivity)
}