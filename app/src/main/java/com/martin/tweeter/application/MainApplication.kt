package com.martin.tweeter.application

import android.app.Application
import com.martin.tweeter.dagger.AppComponent
import com.martin.tweeter.dagger.DaggerAppComponent
import com.martin.tweeter.dagger.modules.AppModule

class MainApplication: Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    @Suppress("DEPRECATION")
    private fun initDagger() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }
}