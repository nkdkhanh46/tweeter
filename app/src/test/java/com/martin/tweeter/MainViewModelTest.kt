package com.martin.tweeter

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.martin.tweeter.features.main.MainViewModel
import org.hamcrest.CoreMatchers.hasItems
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

class MainViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val viewModel = MainViewModel()

    @Test
    fun splitMessage_LongCharWord() {
        val message = "Loremipsumdolorsitamet,consecteturadipiscingelit,seddoeiusmodtemporincididunt ut labore et dolore magna aliqua"
        val splitMessages = viewModel.splitMessage(message)

        assertNull(splitMessages)
    }

    @Test
    fun splitMessage_SingleMessage() {
        val message = "Lorem ipsum dolor sit amet"
        val splitMessages = viewModel.splitMessage(message)

        assertThat(splitMessages, hasItems(message))
    }

    @Test
    fun splitMessage_MultipleMessages() {
        val message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non."
        val messages = ArrayList<String>()
            .apply {
            add("1/9 Lorem ipsum dolor sit amet, consectetur")
            add("2/9 adipiscing elit, sed do eiusmod tempor")
            add("3/9 incididunt ut labore et dolore magna aliqua.")
            add("4/9 Ut enim ad minim veniam, quis nostrud")
            add("5/9 exercitation ullamco laboris nisi ut aliquip")
            add("6/9 ex ea commodo consequat. Duis aute irure dolor")
            add("7/9 in reprehenderit in voluptate velit esse")
            add("8/9 cillum dolore eu fugiat nulla pariatur.")
            add("9/9 Excepteur sint occaecat cupidatat non.")
        }
        val splitMessages = viewModel.splitMessage(message)

        assertNotNull(splitMessages)
        assertEquals(messages, splitMessages)
    }

    @Test
    fun splitMessage_MoreThanNineMessages() {
        val message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        val messages = ArrayList<String>()
            .apply {
            add("1/11 Lorem ipsum dolor sit amet, consectetur")
            add("2/11 adipiscing elit, sed do eiusmod tempor")
            add("3/11 incididunt ut labore et dolore magna aliqua.")
            add("4/11 Ut enim ad minim veniam, quis nostrud")
            add("5/11 exercitation ullamco laboris nisi ut aliquip")
            add("6/11 ex ea commodo consequat. Duis aute irure")
            add("7/11 dolor in reprehenderit in voluptate velit")
            add("8/11 esse cillum dolore eu fugiat nulla pariatur.")
            add("9/11 Excepteur sint occaecat cupidatat non")
            add("10/11 proident, sunt in culpa qui officia deserunt")
            add("11/11 mollit anim id est laborum.")
        }
        val splitMessages = viewModel.splitMessage(message)

        assertNotNull(splitMessages)
        assertEquals(messages, splitMessages)
    }
}